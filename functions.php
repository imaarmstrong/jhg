<?php
require_once get_template_directory() . '/inc/acf/options.php';
require_once get_template_directory() . '/inc/acf/local.php';
require_once get_template_directory() . '/inc/cpt.php';

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_theme_support('post-thumbnails');

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'jhg' ),
) );

if ( ! file_exists( get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php' ) ) {
    // File does not exist... return an error.
    return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
    // File exists... require it.
    require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
}

function bootstrap_styles() {
    wp_enqueue_style('bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css');
     wp_enqueue_style( 'jhg', get_template_directory_uri() . '/dist/css/main.css' );
    wp_enqueue_script( 'boot1','https://code.jquery.com/jquery-3.3.1.slim.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot2','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot3','https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/dist/js/all.js?defer', array ( 'jquery' ), 1.1, true);
}
add_action( 'wp_enqueue_scripts', 'bootstrap_styles' );



?>
