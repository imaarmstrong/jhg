<?php
// Register Custom Post Type
function linkboxes() {

	$labels = array(
		'name'                  => _x( 'Linkboxes', 'Post Type General Name', 'jhg' ),
		'singular_name'         => _x( 'Linkbox', 'Post Type Singular Name', 'jhg' ),
		'menu_name'             => __( 'Linkboxes', 'jhg' ),
		'name_admin_bar'        => __( 'Linkbox', 'jhg' ),
		'archives'              => __( 'Item Archives', 'jhg' ),
		'attributes'            => __( 'Item Attributes', 'jhg' ),
		'parent_item_colon'     => __( 'Parent Item:', 'jhg' ),
		'all_items'             => __( 'All Items', 'jhg' ),
		'add_new_item'          => __( 'Add New Item', 'jhg' ),
		'add_new'               => __( 'Add New', 'jhg' ),
		'new_item'              => __( 'New Item', 'jhg' ),
		'edit_item'             => __( 'Edit Item', 'jhg' ),
		'update_item'           => __( 'Update Item', 'jhg' ),
		'view_item'             => __( 'View Item', 'jhg' ),
		'view_items'            => __( 'View Items', 'jhg' ),
		'search_items'          => __( 'Search Item', 'jhg' ),
		'not_found'             => __( 'Not found', 'jhg' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'jhg' ),
		'featured_image'        => __( 'Featured Image', 'jhg' ),
		'set_featured_image'    => __( 'Set featured image', 'jhg' ),
		'remove_featured_image' => __( 'Remove featured image', 'jhg' ),
		'use_featured_image'    => __( 'Use as featured image', 'jhg' ),
		'insert_into_item'      => __( 'Insert into item', 'jhg' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'jhg' ),
		'items_list'            => __( 'Items list', 'jhg' ),
		'items_list_navigation' => __( 'Items list navigation', 'jhg' ),
		'filter_items_list'     => __( 'Filter items list', 'jhg' ),
	);
	$args = array(
		'label'                 => __( 'Linkbox', 'jhg' ),
		'description'           => __( 'Linkboxes', 'jhg' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'linkboxes', $args );

}
add_action( 'init', 'linkboxes', 0 );
