<?php get_header(); ?>
  <div class="main-content">
    <div class="hero" style="background-image:url('<?php echo get_field('desktop_image');?>')" >
      <div class="container">
        <div class="hero__content <?php if (get_field('hero_title_hide')): ?> d-md-block d-none<?php else: ?> d-md-none d-block <?php endif; ?>">
          <h1 class="hero__content--title"><?php echo get_field('hero_title'); ?></h1>
          <a class="button bubble-float-right">Read More</a>
        </div>
      </div>
    </div>
    <div class="full-content">
      <div class="container">
        <div class="<?php if (get_field('hero_title_hide')): ?> d-md-none d-block text-center<?php else: ?> d-md-block d-none <?php endif; ?>">
          <h1 class="hero__content--title"><?php echo get_field('hero_title'); ?></h1>
        </div>
        <div class="full-content__content">
          <?php echo get_field('content'); ?>
        </div>
        <div class="<?php if (get_field('hero_title_hide')): ?> d-md-none d-block mt-4 text-center<?php else: ?> d-md-block d-none <?php endif; ?>">
            <a class="button bubble-float-right">Read More</a>
        </div>
      </div>
    </div>
    <div class="linkboxes">
      <div class="container">
        <div class="row no-gutters">
          <?php
          $loop = new WP_Query( array(
              'post_type' => 'linkboxes',
              'posts_per_page' => -1,
              'order'  => 'ASC'
            )
          );
          ?>

          <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="col-md-4 col-12 linkbox">
              <div class="row no-gutters">
                <div class="col-12 <?php if (get_field('swap_columns')): ?>order-md-2 order-1<?php endif; ?>">
                  <figure class="linkboxes--feature-image">
                    <img src="<?php echo the_field('featured_image'); ?>" alt="">
                  </figure>
                </div>
                <div class="col-12 <?php if (get_field('swap_columns')): ?>order-md-1 order-2<?php endif; ?> linkbox-content">
                  <div class="linkboxes--content">
                    <img class="linkboxes--icon" src="<?php echo the_field('icon'); ?>" alt="">
                    <h3 class="linkbox--title"><?php echo the_title(); ?></h3>
                    <div class="linkbox--content">
                      <?php echo the_field('content_for_linkbox'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endwhile; wp_reset_query(); ?>
        </div>
      </div>
    </div>
  </div>
<?php get_footer(); ?>
