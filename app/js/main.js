var ll = $('div');
var lh = []
var wscroll = 0;
var wh = $(window).height();

function update_offsets(){
  $('div').each(function(){
    var x = $(this).offset().top;
    lh.push(x);
  });
};

function lazy() {
  wscroll = $(window).scrollTop();
  for(i = 0; i < lh.length; i++){
    if(lh[i] <= wscroll + (wh - 200)){
      $('div').eq(i).addClass('loaded');
    };
  };
};

// Page Load
update_offsets();
lazy();

$(window).on('scroll',function(){
  lazy();
});

$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 250) {
        $(".header").addClass("scrolled");
    } else {
        $(".header").removeClass("scrolled");
    }
});
