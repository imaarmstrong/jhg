##JHG | PLOTT Creative Development Task

### Ashley Armstrong

## Dev Setup Time: 40mins

So the reason why it took so long to setup is due to me reinstalling my system and having to reinstall everything.

### Created in Local by Flywheel

Site URL: jhg.local

### Login to the backend
Username: admin
Password: admin

### Folder Setup

###app  
####--scss  
#####---deafults  
#####---parts  
####--js  
###dist  
####--img  
####--css  
####--js  
###-inc  
####--acf  


## Node Modules Used (found in gulpfile.js)

*gulp-sourcemaps*  
*gulp-sass*  
*gulp-concat*  
*gulp-uglif*  
*gulp-postcss*  
*autoprefixer*  
*cssnano*  
*gulp-replace*  

## The Approach

I used ACF to create the fields needed to be able to change parts of the site to appear on Mobile and Desktop. Also used True or False fields for example in the linkboxes to determine the position for the image responsively.

In the way of images and loading content there is a lady load function in the javascript and also inside there is a function to add a class on to the header after 350px has been scrolled, which will then allow the header and the logos (both Mobile and Desktop) to shrink to allow the end user too see more content, this is animated with CSS.

I have loaded the JSON files locally within the '/acf-json' folder, this will allow the backend of the site nice and clean, the only plugin used is ACF. The includes folder holds an ACF folder, which has 'local.php' and 'options.php'. 'local.php' loads all the custom fields into the '/acf-json' folder and then the options.php allows you change the logos.

Also in the includes folder is a Boostrap Nav Walker and 'cpt.php' file the 'cpt.php' loads the Custom Post Type, which I've done the linkboxes in.
