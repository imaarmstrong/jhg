<footer>
  <div class="footer__copy">
    <div class="container mx-auto">
      <p class="footer__copy--text">&copy; <?php echo date("Y"); ?> <?php echo get_bloginfo('name'); ?> </p>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
