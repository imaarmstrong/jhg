<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo get_bloginfo('name'); ?></title>
    <?php wp_head(); ?>
  </head>
  <body>
    <header class="header header-colour__<?php echo get_field('header_colour', 'options'); ?>">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-8">
            <a href="<?php echo get_site_url(); ?>" class="d-lg-block d-none">
              <figure class="header-logo">
                <?php echo wp_get_attachment_image(get_field('desktop_logo', 'options')); ?>
              </figure>
            </a>
            <a href="<?php echo get_site_url(); ?>" class="d-lg-none d-block">
              <figure class="header-logo__mobile">
                <?php echo wp_get_attachment_image(get_field('mobile_logo', 'options')); ?>
              </figure>
            </a>
          </div>
          <div class="col-md-6 col-4">
            <nav class="navbar-expand-lg text-right">
              <button type="button" class="navbar-toggle collapsed d-md-none d-block" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

              <?php
                wp_nav_menu( array(
                  'theme_location'  => 'primary',
                  'depth'           => 1, // 1 = no dropdowns, 2 = with dropdowns.
                  'container'       => 'div',
                  'container_class' => 'collapse navbar-collapse navbar-expand-md',
                  'container_id'    => 'bs-example-navbar-collapse-1',
                  'menu_class'      => 'navbar-nav mr-auto',
                  'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                  'walker'          => new WP_Bootstrap_Navwalker(),
                ) );
              ?>
            </nav>
          </div>
        </div>
      </div>
    </header>
